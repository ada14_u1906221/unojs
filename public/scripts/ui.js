define('ui',
    [
        'jquery',
        'backbone',
        'data',
        'views/partides/list',
        'views/usuaris/list',
        'views/partides/joc',
        'socket'
    ], function($, Backbone, Data, PartidaView, UsuarisView, JocView,io) {
    var Ui = {};

    var partidesView = new PartidaView({el: '#partida_view'});
        var usuarisView = new UsuarisView({el: '#usuaris_view'});
        var socket = io.connect();

    Ui.initialize = function() {
        Ui.updatePartides();
    };



    Ui.updatePartides = function() {
        $("#all").show();
        partidesView.update()
        partidesView.$el.show()
        usuarisView.$el.hide()
        //testView.$el.hide()
    };

    Ui.updateUsuaris = function() {
        $("#all").show();
        partidesView.$el.hide()
        usuarisView.update()
        usuarisView.$el.show()
        //testView.$el.hide()
    };


    return Ui;

});
