/**
 * Created by Javier on 23/04/2014.
 */
define([
    'underscore',
    'backbone'
], function(_, Backbone){
    var PartidaModel = Backbone.Model.extend({
        urlRoot: "/partides"

        /*url : function() {
            var base = '/partides';
            if (this.isNew()) return base;
            return base + (base.charAt(base.length - 1) == '/' ? '' : '/') + this.id;
        }*/
    });
    // Return the model for the module
    return PartidaModel;
});