/**
 * Created by salvat on 09/05/14.
 */
define([
    'underscore',
    'backbone'
], function(_, Backbone){
    var UsuariModel = Backbone.Model.extend({
        urlRoot: "/usuari"
    });
    // Return the model for the module
    return UsuariModel;
});