/**
 * Created by Javier on 14/05/2014.
 */
define('globals', ['socket'], function(socket) {

    var globals = {};

    globals.socket = socket.connect();

    return globals;
});