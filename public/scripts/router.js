define([
    'jquery',
    'underscore',
    'backbone'
], function ($, _, Backbone) {

    var AppRouter = Backbone.Router.extend({
        routes: {
            // Define some URL routes
            'partides': 'showPartides',
            'usuaris' : 'showUsuaris',
            'logout' : 'logout',
            // Default
            '*actions': 'defaultAction'
        }
    });

    var app_router = new AppRouter();

    var initialize = function (Ui) {

        app_router.on('route:showPartides', function () {
            Ui.updatePartides();
        });

        app_router.on('route:showUsuaris', function () {
            Ui.updateUsuaris();
        });

        app_router.on('route:logout', function() {
            $.ajax({
                url : "/logout",
                type : "DELETE",
                dataType: "json",
                complete : function() {
                    window.location.href = "/login.html";
                }
            });
        });

        app_router.on('defaultAction', function (actions) {
            // We have no matching route, lets just log what the URL was
            console.log('No route:', actions);
        });
        Backbone.history.start();
    };

    return {
        initialize: initialize,
        navigate: function(hash) {
            app_router.navigate(hash, {trigger: true})
        }
    };
});