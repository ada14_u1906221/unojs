/**
 * Created by salvat on 09/05/14.
 */
define([
    'jquery',
    'underscore',
    'backbone',
    'collections/usuaris',
    'models/usuari',
    // Using the Require.js text! plugin, we are loaded raw text
    // which will be used as our views primary template
    'text!/templates/usuaris/list.html'
], function($, _, Backbone, UsuariCollection, UsuariModel, usuarisListTemplate) {
    var ProjectListView = Backbone.View.extend(function() {

        var usuaris = []; //new UsuariCollection();


        return {

            events: {
                //'click a.showCreaPartida' : 'showCreaPartida',
                'click button.submit' : 'buscarUsuari'
                //'click tr' : 'showPartida'

            },


            initialize: function (params) {
                //var that = this;
                this.render();
                /*usuaris.on('reset', function () {
                    that.render(usuaris)
                })*/
            },

            render: function () {
                // Using Underscore we can compile our template with data
                var compiledTemplate = _.template(usuarisListTemplate, {items: usuaris});
                // Append our compiled template to this Views "el"
                this.$el.empty().append(compiledTemplate);
            },

            update: function (options) {
                /*var op = options || {reset: true}
                usuaris.fetch(op);
                this.render();*/

            },
            buscarUsuari: function(){
                //var tr = $(e.currentTarget);
                //alert(1);
                var params = {};
                var form = this.$el.find("#cerca");
               // var input = form.find("input[name='nick']");
                //params['nick'] = form.val();

                var that = this;
                //var usuari = usuaris.fetch({cerca : params.nick,reset: true});
                //usuarisView.initialize({usuari : usuari});
                $.ajax({
                    url: "/cerca",
                    data: {cerca : form.val()},
                    success: function(data) {
                        //console.log(data)
                        usuaris=data;
                        that.render();
                    },
                    error : function(err) {
                        console.log(err.responseText);
                    },

                    dataType: "json"
                });

            }


        }
    }());

    // Our module now returns our view
    return ProjectListView;
});