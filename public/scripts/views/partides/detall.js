/**
 * Created by Javier on 09/05/2014.
 */
define([
    //'jquery',
    'jquery-ui',
    'underscore',
    'backbone',
    'models/partida',
    'text!/templates/partides/detall.html',
    'views/partides/joc'
], function($, _, Backbone, PartidaModel, partidesListTemplate,jocView) {
    var PartidaView = Backbone.View.extend(function() {

        var partida = null;
        var participants = [];
        var isJoined = false;
        var delegate;
        var that;
        var socket;
        return {

            events: {
                'click .btn-join' : 'join',
                'click .btn-disjoin' : 'disjoin'
            },

            initialize : function(params) {
                if (!that) that = this;
                if (!delegate) delegate = params.delegate;

                this.init(params.partida);

            },

            init : function (p) {
                partida = p;
                var url = "/partides/" + partida.id + "/ijoined";
                socket = io.connect();
                socket.heartbeatTimeout=20000;
                socket.on("connect", function(){
                   socket.on("disconnect", function() {
                       console.log("s'ha desconectat")
                   });
                });
                $.get(url, function(data) {
                    isJoined = data;
                    if(isJoined){
                        socket.emit('join', { idPartida: partida.id });
                        socket.on('inici', function (data) {
                            that.obrirPartidaView();
                        });
                    }


                    that.update();
                    /*
                    $("#all").hide();
                    var partidaView = new jocView({el:"#joc_view", partida : partida, socket : socket });
                    */
                });
            },

            render: function () {
                var compiledTemplate = _.template(partidesListTemplate, {partida: partida, jugadors : participants});
                that.$el.empty().append(compiledTemplate);

                var closeButton;
                if(isJoined) {
                    that.$el.find(".btn-join").hide();
                    that.$el.find(".btn-disjoin").show();
                    closeButton = 'no-close';
                } else {
                    that.$el.find(".btn-join").show();
                    that.$el.find(".btn-disjoin").hide();
                    closeButton = '';
                }

                that.$el.dialog({
                    //closeOnEscape: false,
                    resizable: false,
                    modal: true,
                    width:'90%',
                    dialogClass: closeButton
                });
             },

            update: function () {
                var url = "/partides/" + partida.id + "/participants";
                $.ajax({
                    url : url,
                    type: 'GET',
                    dataType: "json",
                    success : function(data) {
                        participants = data;
                        that.render();
                    },
                    error : function(err) {
                        console.log(err.responseText);
                        //that.$el.hide();
                        delegate.update();
                    }
                });
            },

            join : function() {
                $.ajax({
                    type : "PUT",
                    url : "/partides/" + partida.id + "/join",
                    dataType: "json",
                    success : function(msg) {
                        isJoined = true;

                        socket.emit('join', { idPartida: partida.id });
                        if (!msg.tots) {
                            socket.on('inici', function (data) {
                                that.obrirPartidaView();
                            });

                            that.update();
                        } else {
                            that.obrirPartidaView();
                        }


                    },
                    error : function(err) {
                        alert(err.responseText);
                    }
                });

            },

            disjoin : function() {
                $.ajax({
                    type : "PUT",
                    url : "/partides/" + partida.id + "/disjoin",
                    dataType: "json",
                    success : function(msg) {
                        isJoined = false;
                        that.$el.dialog('close');
                        socket.emit("disjoin", {idPartida:partida.id});
                        delegate.update();
                    },
                    error : function(err) {
                        alert(err.responseText);
                    }
                });
            },

            obrirPartidaView : function () {
                console.log("inici!");
                var partidaView = new jocView({el:"#joc_view", partida : partida, socket : socket });
                $("#all").hide();
                that.$el.dialog("close");
            }

        }
    }());

    return PartidaView;
});