/**
 * Created by Javier on 23/04/2014.
 */
define([
    'jquery',
    'underscore',
    'backbone',
    'model/partida',
    // Using the Require.js text! plugin, we are loaded raw text
    // which will be used as our views primary template
    'text!/templates/partides/crea.html'
], function($, _, Backbone, PartidaModel, creaTemplate) {
    var ProjectListView = Backbone.View.extend(function() {

        var partides = new PartidaCollection();

        return {

            events: {
                'click button.submit' : 'desaPartida'
            },

            initialize: function (params) {
            },

            render: function () {
                // Using Underscore we can compile our template with data
                var compiledTemplate = _.template(creaTemplate);
                // Append our compiled template to this Views "el"
                this.$el.empty().append(compiledTemplate);
            },

            desaPartida : function () {
                console.log("pitxaaa");
            }
        }
    }());

    // Our module now returns our view
    return ProjectListView;
});