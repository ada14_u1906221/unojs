/**
 * Created by Javier on 14/05/2014.
 */
define([
    'jquery',
    'underscore',
    'backbone',
    'socket',
    'models/partida',
    'text!/templates/partides/joc.html'
], function($, _, Backbone, io, PartidaModel, jocTemplate) {
    var JocView = Backbone.View.extend(function() {

        var that;
        var socket;
        var partida;
        var id;
        var cartesJugador;
        var cartaTiradaUltima;
        var haTirat;
        var idTeu;
        var nicks;
        var cartes = {
            "R0" : "images/cartes/vermell/0.jpg",
            "R1" : "images/cartes/vermell/1.jpg",
            "R2" : "images/cartes/vermell/2.jpg",
            "R3" : "images/cartes/vermell/3.jpg",
            "R4" : "images/cartes/vermell/4.jpg",
            "R5" : "images/cartes/vermell/5.jpg",
            "R6" : "images/cartes/vermell/6.jpg",
            "R7" : "images/cartes/vermell/7.jpg",
            "R8" : "images/cartes/vermell/8.jpg",
            "R9" : "images/cartes/vermell/9.jpg",
            "RI" : "images/cartes/vermell/CanviSentit.jpg",
            "RD" : "images/cartes/vermell/Mes2.jpg",
            "RS" : "images/cartes/vermell/Skip.jpg",

            "G0" : "images/cartes/verd/0.jpg",
            "G1" : "images/cartes/verd/1.jpg",
            "G2" : "images/cartes/verd/2.jpg",
            "G3" : "images/cartes/verd/3.jpg",
            "G4" : "images/cartes/verd/4.jpg",
            "G5" : "images/cartes/verd/5.jpg",
            "G6" : "images/cartes/verd/6.jpg",
            "G7" : "images/cartes/verd/7.jpg",
            "G8" : "images/cartes/verd/8.jpg",
            "G9" : "images/cartes/verd/9.jpg",
            "GI" : "images/cartes/verd/CanviSentit.jpg",
            "GD" : "images/cartes/verd/Mes2.jpg",
            "GS" : "images/cartes/verd/Skip.jpg",

            "B0" : "images/cartes/blau/0.jpg",
            "B1" : "images/cartes/blau/1.jpg",
            "B2" : "images/cartes/blau/2.jpg",
            "B3" : "images/cartes/blau/3.jpg",
            "B4" : "images/cartes/blau/4.jpg",
            "B5" : "images/cartes/blau/5.jpg",
            "B6" : "images/cartes/blau/6.jpg",
            "B7" : "images/cartes/blau/7.jpg",
            "B8" : "images/cartes/blau/8.jpg",
            "B9" : "images/cartes/blau/9.jpg",
            "BI" : "images/cartes/blau/CanviSentit.jpg",
            "BD" : "images/cartes/blau/Mes2.jpg",
            "BS" : "images/cartes/blau/Skip.jpg",

            "Y0" : "images/cartes/groc/0.jpg",
            "Y1" : "images/cartes/groc/1.jpg",
            "Y2" : "images/cartes/groc/2.jpg",
            "Y3" : "images/cartes/groc/3.jpg",
            "Y4" : "images/cartes/groc/4.jpg",
            "Y5" : "images/cartes/groc/5.jpg",
            "Y6" : "images/cartes/groc/6.jpg",
            "Y7" : "images/cartes/groc/7.jpg",
            "Y8" : "images/cartes/groc/8.jpg",
            "Y9" : "images/cartes/groc/9.jpg",
            "YI" : "images/cartes/groc/CanviSentit.jpg",
            "YD" : "images/cartes/groc/Mes2.jpg",
            "YS" : "images/cartes/groc/Skip.jpg",

            "NC" : "images/cartes/comodins/CanviColor.jpg",
            "NQ" : "images/cartes/comodins/Mes4.jpg"

        };
        return {
            events: {
                'click button.submit' : 'enviar_mssg',
                'keyup #enviar_xat' : 'processKey',
                'click button.botoUno' : 'una_carta',
                'click #baralla' : 'clickCarta',
                'click #piloAgafar' : 'clickPilo'

            },

            initialize: function (params) {
                that= this;
                socket = params.socket;
                partida = params.partida;
                id = params.el;

                socket.on('xat', function(data){
                    $('#text_xat').append(data.nick + ": "+ data.mssg +"<br/>");
                    $("#text_xat").scrollTop($("#text_xat")[0].scrollHeight);
                });
                socket.on('afegirCartes', function(data){
                   cartesJugador=data.cartes;
                    that.mostrarCartes();
                });
                socket.on('uno',function(data){
                    jNotify(
                        'UNOOOOO!!!',
                        {
                            autoHide : true, // added in v2.0
                            clickOverlay : true, // added in v2.0
                            MinWidth : 250,
                            TimeShown : 3000,
                            ShowTimeEffect : 200,
                            HideTimeEffect : 200,
                            LongTrip :20,
                            HorizontalPosition : 'center',
                            VerticalPosition : 'center',
                            ShowOverlay : true,
                            ColorOverlay : '#000',
                            OpacityOverlay : 0.3,
                            onClosed : function(){ // added in v2.0

                            },
                            onCompleted : function(){ // added in v2.0

                            }
                        });
                });
                socket.on('cartesRepartides',function(data){
                    nicks = data.usuaris;
                    cartesJugador=data.cartesRepartides;
                    that.mostrarCartes();
                    idTeu=data.idUsuari;

                    that.mostrarTorn(data.jugadorTirar);
                    that.mostrarJugadors(data.usuaris);
                });
                socket.on('error',function(data){
                   if(data.error) {
                       alert(data.error);
                   }
                    haTirat = false;
                    cartaTiradaUltima = "";
                });
                socket.on('continua',function(data){
                    if(haTirat){
                        cartesJugador= _.without(cartesJugador, _.findWhere(cartesJugador, cartaTiradaUltima));
                        that.mostrarCartes();
                    }

                    that.mostrarTorn(data.jugadorTirar);
                    that.mostrarJugadors(data.usuaris);
                    $('#piloTirades').empty();
                    var pathTirada = cartes[data.carta];
                    var aleatoria = Math.floor((Math.random()*30)-15);
                    var carta = '<img src="'+ pathTirada +'" style="-ms-transform: rotate('+aleatoria+'deg);-webkit-transform: rotate('+aleatoria+'deg);transform: rotate('+aleatoria+'deg);" />' ;

                    $('#piloTirades').append(carta);
                    cartaTiradaUltima = null;
                    haTirat = false;

                    var co;
                    if(data.color=='R')co='Vermell';
                    else if(data.color=='B')co='Blau';
                    else if(data.color=='G')  co='Verd';
                    else if (data.color=='Y')co='Groc';
                    if(co) {
                        jNotify(
                                'Canvi de color a ' + co + '!!',
                            {
                                autoHide: true, // added in v2.0
                                clickOverlay: true, // added in v2.0
                                MinWidth: 250,
                                TimeShown: 3000,
                                ShowTimeEffect: 200,
                                HideTimeEffect: 200,
                                LongTrip: 20,
                                HorizontalPosition: 'center',
                                VerticalPosition: 'center',
                                ShowOverlay: true,
                                ColorOverlay: '#000',
                                OpacityOverlay: 0.3,
                                onClosed: function () { // added in v2.0

                                },
                                onCompleted: function () { // added in v2.0

                                }
                            });
                    }

                });
                socket.on('fi',function(data){
                    if(data.idUsuari==idTeu){
                        $('#guanyador').dialog({
                            close : function(event, ui){
                                $("#all").show();
                                that.$el.html("");
                            }
                        });
                        $('#guanyador p').html("FELICITATS!! HAS GUANYAT");
                    }
                    else {
                        $('#guanyador').dialog({
                            close : function(event, ui){
                                $("#all").show();
                                that.$el.html("");
                            }
                        });
                        var nom = "";
                        for (var i = 0; i < nicks.length; i++) {
                            if (nicks[i].idUsuari == data.idUsuari) {

                                nom = nicks[i].nick;
                                break;
                            }
                        }
                        $('#guanyador p').html("HA GUANYAT L'USUARI "+nom+"!! LOOOOSEEEERRR");
                    }
                });
                this.render();
            },

            mostrarJugadors : function(usuaris){
                var quantsJugadors=usuaris.length;
                var count=1;
                for(var i=0;i<quantsJugadors; i++){
                    if(usuaris[i].idUsuari==idTeu){
                        if(usuaris[i].nick) {
                            $('#Tu p').html(usuaris[i].nick);
                        }
                    }
                    else{

                        if(usuaris[i].nick){
                            $('#Jugador'+count+' span').html(usuaris[i].nick);


                        }
                        $('#Jugador'+count+' p').html(usuaris[i].nCartes);
                        count++;
                    }
                }
            },

            mostrarTorn : function(idTorn){
                if(idTorn == idTeu){
                    $('#torn').html('És el teu torn');
                }
                else{
                    for (var i = 0; i < nicks.length; i++) {
                        if (nicks[i].idUsuari == idTorn) {
                            $('#torn').html('És el torn de ' + nicks[i].nick);
                        }

                    }
                }

            },
            clickCarta : function(e){
                var luke = $(e.target);
                var frodo = luke.data("carta");

                if(!frodo) return;

                haTirat = true;
                if(frodo == 'NC' || frodo=='NQ'){
                    $('#escullColor').dialog({
                        width: 800
                    });
                }
                else socket.emit('tirarCarta',{idPartida: partida.id, carta: frodo});
                cartaTiradaUltima=frodo;

            },

            clickPilo : function(){
                socket.emit('agafarCartes',{idPartida: partida.id});

            },
            mostrarCartes : function(){
                $('#baralla').empty();
                var carta="";
                cartesJugador.sort();
                for(var i=0; i<cartesJugador.length; i++){
                    var pathCarta = cartes[cartesJugador[i]];

                    carta+=' <img src="'+ pathCarta +'" data-carta="'+cartesJugador[i]+'" />' ;
                }
                $('#baralla').append(carta);
            },

            render : function() {

                var compiledTemplate = _.template(jocTemplate, {partida: partida});

                console.log(compiledTemplate);
                this.$el.empty().append(compiledTemplate);
                this.$el.show();
                $('#botoBlau').click(function(){
                    that.escollirColor("B");
                });
                $('#botoVermell').click(function(){
                    that.escollirColor("R");
                });
                $('#botoGroc').click(function(){
                    that.escollirColor("Y");
                });
                $('#botoVerd').click(function(){
                    that.escollirColor("G");
                });
                $("#baralla").mousewheel(function(event, delta) {
                    this.scrollLeft -= (delta * 30);
                    event.preventDefault();
                });


            },
            enviar_mssg:function(){
                var missatg = $('#enviar_xat').val();
                socket.emit('xat',{idPartida: partida.id, mssg: missatg})
                $('#enviar_xat').val("");

            },
            processKey : function(e){
                if(e.which === 13) this.enviar_mssg();

            },
            una_carta : function(){
                socket.emit('uno',{idPartida: partida.id});
            },
            escollirColor : function (e){
                $('#escullColor').dialog('close');
                socket.emit('tirarCarta',{idPartida: partida.id, carta: cartaTiradaUltima,color : e});

            }
        }

    }());

    // Our module now returns our view
    return JocView;
});