define([
    'jquery',
    'underscore',
    'backbone',
    'collections/partides',
    'models/partida',
    'text!/templates/partides/list.html',
    'views/partides/detall'
], function($, _, Backbone, PartidaCollection, PartidaModel, partidesListTemplate, PartidaDetallView) {
    var ProjectListView = Backbone.View.extend(function() {

        var partides = new PartidaCollection();
        var partidaView;
        var creada = false;
        var that;
        return {

            events: {
                'click a.showCreaPartida' : 'showCreaPartida',
                'click button.submit' : 'desaPartida',
                'click tr' : 'showPartida'
            },

            obrePartidaDetall : function(idPartida) {
                var partida = partides.get(idPartida);
                if(!partidaView)
                    partidaView = new PartidaDetallView({el:".detall_partida", partida : partida, delegate: this});
                else
                    partidaView.init(partida);
            },

            showPartida : function(e) {
                var tr = $(e.currentTarget);
                var span = tr.find(".idPartida");
                var idPartida = span.html();
                that.obrePartidaDetall(idPartida);
            },

            initialize: function (params) {
                that = this;
                partides.on('reset', function () {
                    that.render(partides);
                    if (creada) {
                        var partida = partides.findWhere({nom : creada});
                        creada = false;
                        that.obrePartidaDetall(partida.id);
                    }
                })
            },

            render: function () {
                // Using Underscore we can compile our template with data
                var compiledTemplate = _.template(partidesListTemplate, {items: partides});
                // Append our compiled template to this Views "el"
                that.$el.empty().append(compiledTemplate);
            },

            update: function (options) {
                var op = options || {reset: true}
                partides.fetch(op);
            },

            desaPartida : function () {
                var divError = that.$el.find(".error_crear");
                divError.hide();
                var params = {};
                var form = that.$el.find("#form_partida");
                var input = form.find("input[name='nom']");
                params['nom'] = input.val();
                var select = form.find("select[name='max_jugadors']");
                params['max_jugadors'] = select.val();

                //var that = this;
                var partida = new PartidaModel(params);
                partida.save(null, {
                    success : function (model, response, options) {
                        creada = model.get('nom');
                        that.update();
                    },
                    error : function(m,o) {
                        divError.show();
                        var res = JSON.parse(o.responseText);
                        divError.find(".msgErrorCrearPartida").html(res.error);
                    }});

            },

            showCreaPartida : function () {

                var form_partida = that.$el.find("#form_partida");
                if (form_partida.is(':visible'))
                    form_partida.hide();
                else
                    form_partida.show();
                return false;
            }
        }
    }());

    // Our module now returns our view
    return ProjectListView;
});