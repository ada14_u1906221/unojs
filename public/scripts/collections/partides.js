/**
 * Created by Javier on 23/04/2014.
 */
define([
    'backbone',
    // Pull in the Model module from above
    'models/partida'
], function(Backbone, PartidaModel){
    var PartidaCollection = Backbone.Collection.extend({
        model: PartidaModel,
        url: "/partides"
    });
    // You don't usually return a collection instantiated
    return PartidaCollection;
});