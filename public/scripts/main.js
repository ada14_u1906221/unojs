require.config({
    paths: {
        jquery: 'libs/jquery-1.11.0.min',
        "jquery.bootstrap": "libs/bootstrap.min",
        "jquery-ui" : "libs/jquery-ui-1.10.4.min",
        underscore: 'libs/underscore-min',
        backbone: 'libs/backbone-min',
        "socket" : "/socket.io/socket.io",
        "jnotify" : "libs/jNotify.jquery.min",
        "mousewheel" : "libs/jquery.mousewheel"
    },
    shim : {
        "jquery-ui" : {
            exports : "$",
            deps : ["jquery", "jnotify", "mousewheel"]
        }
    }

});

require([

    // Load our app module and pass it to our definition function
    'app'
], function(App){
    // The "app" dependency is passed in as "App"
    App.initialize();
    //App.navigate("shops")
});