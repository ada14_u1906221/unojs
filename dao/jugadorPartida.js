/**
 * Created by Javier on 18/04/2014.
 */

module.exports = function (db) {
    var dao = {};
    var W = require('when');
    var util = require('../util');

    dao.create = function(usuari, partida, t) {

        var df = W.defer();
        var options = util.addTrans(t, {});
        var params = {
            PartidaId: partida.id,
            UsuariId: usuari.id
        };
        db.JugadorPartida.create(params, options).success(df.resolve).error(df.reject);
        return df.promise;

    },

    dao.numeroJugadors = function (partida, t) {
        var df = W.defer();
        var options = util.addTrans(t, {where: {PartidaId: partida.id}});
        db.JugadorPartida.count(options)
            .success(df.resolve).error(df.reject);
        return df.promise;
    }

    dao.isJoinedUsuari = function (usuari, partida, t) {
        var df = W.defer();
        var options = util.addTrans(t, {where: {UsuariId: usuari.id, PartidaId: partida.id}});
        db.JugadorPartida.find(options)
            .success(df.resolve).error(df.reject);
        return df.promise;
    }
    dao.updateCartes = function (partida, usuari, cartes, t){
        var df = W.defer();
        var options = util.addTrans(t, {where : {UsuariId : usuari.id, PartidaId : partida.id}});
        db.JugadorPartida.find(options)
            .success(function (jp) {
                jp.updateAttributes({cartes : cartes}).success(df.resolve).error(df.reject);
            });
        return df.promise;
    };

    dao.getByPartidaAndUsuari = function (partida, usuari, t) {
        var df = W.defer();
        var options = util.addTrans(t, {where : {UsuariId : usuari.id, PartidaId : partida.id}});
        db.JugadorPartida.find(options)
            .success(df.resolve).error(df.reject);
        return df.promise;
    };

    dao.getByPartida = function (partida, usuari, t) {
        var df = W.defer();
        var options = util.addTrans(t, {where : {PartidaId : partida.id}, order : "ordre ASC"});
        db.JugadorPartida.findAll(options)
            .success(df.resolve).error(df.reject);
        return df.promise;
    };

    dao.setOrdre = function (partida, usuari, ordre, t){
        var df = W.defer();
        var options = util.addTrans(t, {where : {UsuariId : usuari.id, PartidaId : partida.id}});
        db.JugadorPartida.find(options)
            .success(function (jp) {
                jp.updateAttributes({ordre : ordre}).success(df.resolve).error(df.reject);
            });
        return df.promise;
    };

    dao.inici = function (partida, usuari, ordre, cartes, t) {
        var df = W.defer();
        var options = util.addTrans(t, {where : {UsuariId : usuari.id, PartidaId : partida.id}});
        db.JugadorPartida.find(options)
            .success(function (jp) {
                jp.updateAttributes({ordre : ordre, cartes : cartes}).success(df.resolve).error(df.reject);
            });
        return df.promise;
    };
    return dao;
}

