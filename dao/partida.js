/**
 * Created by Javier on 02/04/2014.
 */

module.exports = function (db) {
    var dao = {};
    var W = require('when');
    var util = require('../util');

    dao.create = function (nom, nMaxJugadors, t) {

        if (!nom) {
            throw {message : "Falta el nom de la partida"};
        }

        var atributs = {nom: nom};

        if(nMaxJugadors) {
            atributs = {nom: nom, nMaxJugadors: nMaxJugadors};
        }

        var df = W.defer();
        var options = util.addTrans(t, {});
        db.Partida.create(atributs, options).success(df.resolve).error(df.reject);
        return df.promise;
    }

    dao.getById = function (id, t) {
        var df = W.defer();
        db.Partida.find(util.addTrans(t, {where: {id: id}}))
            .success(df.resolve).error(df.reject);
        return df.promise;
    }

    dao.getByNom = function (nom, t) {
        var df = W.defer();
        var options = util.addTrans(t, {where: {nom: nom}});
        db.Partida.find(options)
            .success(df.resolve).error(df.reject);
        return df.promise;
    }

    dao.getAll = function (t) {
        var df = W.defer();
        var options = {
            attributes: ['partidas.*', ['(SELECT COUNT(*) FROM JugadorPartidas WHERE PartidaId = Partidas.id)', 'NumJugadors']],
            where: "comensada = false"
        };
        db.Partida.findAll(options)
            .success(df.resolve).error(df.reject);
        return df.promise;
    }


    dao.getAllParticipants = function (partida, t) {
        var df = W.defer();

        var params = {
            where : {PartidaId : partida.id},
            include : [ //incloure un left join
                {
                    model : db.Usuari,
                    attributes: ['nick', 'guanyades', 'perdudes'] //atributs a mostrar + id
                }
            ],
            attributes : []  //atributs de JugadorPartida
        };

        db.JugadorPartida.findAll(util.addTrans(t, params))
            .success(df.resolve).error(df.reject);

        return df.promise;
    },
    dao.updateNoRepartides = function(partida,noRepartides,t){

        var df = W.defer();
        var options = util.addTrans(t, {});
        partida.updateAttributes({cartesNoRepartides : noRepartides} , options)
            .success(df.resolve).error(df.reject);
        return df.promise;
    },

     dao.setInici = function (partida, idUsuari, t) {
         var df = W.defer();
         var options = util.addTrans(t, {});
         partida.updateAttributes({UsuariId : idUsuari, comensada: true, cartesTirades: "", cartesEmpassades:0, colorActual:"",sentitHorari:true} , options)
             .success(df.resolve).error(df.reject);
         return df.promise;
     }


    return dao;
}