/**
 * New node file
 */

module.exports = function(db) {
	var dao = {};
	var W = require('when');

    dao.Usuari = require('./usuari')(db);
    dao.Partida = require('./partida')(db);
    dao.JugadorPartida = require('./jugadorPartida')(db);


    dao.create = function(Model, data) {
		var df = W.defer();
		Model.create(data).success(df.resolve).error(df.reject);
		return df.promise;
	}


    return dao;
}