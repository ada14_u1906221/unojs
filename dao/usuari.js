/**
 * Created by salvat on 01/04/14.
 */

module.exports = function (db) {
    var dao = {};
    var W = require('when');
    var util = require('../util');

    dao.create = function (usuari, t) {
        if (!usuari.nick) throw {message: "Falta el nick"};
        var df = W.defer();
        var options = util.addTrans(t, {});
        db.Usuari.create(usuari, options).success(df.resolve).error(df.reject);
        return df.promise;

    }
    dao.getByNick = function (nick, t) {
        var df = W.defer();
        var options = util.addTrans(t, {where: {nick: nick}});
        db.Usuari.find(options)
            .success(df.resolve).error(df.reject);
        return df.promise;
    }
    dao.getUsuari = function(id, t){
        var df= W.defer();
        var options=util.addTrans(t,{where: {id: id}});
        db.Usuari.find({where: {id: id},attributes: ['id', 'nick','ultimaConexio','guanyades','perdudes','nivell']}).success(df.resolve);
        return df.promise;
    }
    dao.getById = function (id, t) {
        var df = W.defer();
        var options = util.addTrans(t, {where: {id: id}});
        db.Usuari.find(id)
            .success(df.resolve).error(df.reject);
        return df.promise;
    }
    dao.getUsuaris = function(nick,t){
        var df= W.defer();
        var options=util.addTrans(t,{});
        db.Usuari.findAll({where : "nick  like '%"+nick+"%'",attributes: ['nick','ultimaConexio','guanyades','perdudes','nivell']}).success(df.resolve).error(df.reject);
        return df.promise;
    }
    dao.update = function(usuari,contrasenya,t){
        var df = W.defer();
        var options = util.addTrans(t,{})
        usuari.updateAttributes({contrasenya: contrasenya}, options).success(df.resolve).error(df.reject);
        return df.promise;
    }
    dao.afegirAmic = function(usuari,usuariAmic,t){
        var df = W.defer();
        var options = util.addTrans(t,{});
        db.Amic.create({UsuariId1 : usuari.id, UsuariId2 : usuariAmic.id},options).success(df.resolve).error(df.reject);
        return df.promise;
    }
    dao.getAll = function (t) {
        var df = W.defer();
        db.Usuari.findAll(util.addTrans(t,{attributes: ['id', 'nick','ultimaConexio','guanyades','perdudes','nivell']}))
            .success(df.resolve)
            .error(df.reject);
        return df.promise;
    }
    dao.afegirBloquejat = function(usuari,usuariBloc,t){
        var df = W.defer();
        var options = util.addTrans(t,{});
        db.Bloquejat.create({UsuariId1 : usuari.id, UsuariId2 : usuariBloc.id},options).success(df.resolve).error(df.reject);
        return df.promise;
    }
    dao.eliminarAmic = function(usuari,usuariAmic,t){
        var df = W.defer();
        var options = util.addTrans(t,{});
        db.Amic.find({where: {UsuariId1 : usuari.id, UsuariId2 : usuariAmic.id}})
            .success(function (amic){
                amic.destroy().success(df.resolve);
            })
            .error(df.reject);
        return df.promise;
    }
    dao.eliminarBloquejat = function(usuari,usuariBloq,t){
        var df = W.defer();
        var options = util.addTrans(t,{});
        db.Bloquejat.find({where: {UsuariId1 : usuari.id, UsuariId2 : usuariBloq.id}})
            .success(function (bloquejat){
                bloquejat.destroy().success(df.resolve);
            })
            .error(df.reject);
        return df.promise;
    }
    return dao;
}
