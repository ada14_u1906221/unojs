var express = require('express')
    , http = require('http')
    , path = require('path')
    , connect = require('connect')
var cors = require('cors');
var Sockets = require('./util/sockets');
var JocController = require('./util/JocController');


var app = express()

app.set('port', process.env.PORT || 3000)
app.set('views', __dirname + '/views')
app.set('view engine', 'jade')
app.use(express.favicon())
app.use(express.logger('dev'))
app.use(express.json())
app.use(express.urlencoded())
app.use(express.methodOverride())
app.use(cors());
var cookieParser = express.cookieParser('AnnaXeviUnoEinf2014')
app.use(cookieParser);
sessionStore = new connect.middleware.session.MemoryStore();
app.use(express.session({ store: sessionStore }));
app.use(app.router)
app.use(express.static(path.join(__dirname, 'public')))


Function.prototype.genFuncLeft = function () {
    var fn = this, args = Array.prototype.slice.call(arguments);
    return function () {
        return fn.apply(null, args.concat(Array.prototype.slice.call(arguments)));
    }
}

Function.prototype.genFuncRight = function () {
    var fn = this, args = Array.prototype.slice.call(arguments);
    return function () {
        return fn.apply(null, Array.prototype.slice.call(arguments).concat(args));
    }
}



var db = require('./models')(app);

db.initPromise
    .then(function () {

        db.JugadorPartida.destroy();
        db.sequelize.query("UPDATE partidas SET comensada = false");

        var usuaris= require('./routes/usuari')(db)
        var partides = require('./routes/partida')(db);

        function loggedIn(req, res, next) {
            if (req.session.idusuari) {
                next();
            } else {
                console.log("No se qui ets! Identifica't!");
                res.redirect("/login.html");
            }
        }

        app.post('/login', usuaris.login);
        app.delete('/logout', usuaris.logout);
        app.post('/usuaris', usuaris.create);
        app.get('/cerca', usuaris.cercaNick);
        app.put('/usuaris/:id',usuaris.modificarContrasenya);
        app.get('/usuaris/:id',usuaris.getUsuari);
        app.post('/usuaris/:id/amic', usuaris.afegirAmic);
        app.get('/usuaris', usuaris.getAll);
        app.post('/usuaris/:id/bloquejat',usuaris.afegirBloquejat);
        app.delete('/usuaris/:id/amic',usuaris.eliminarAmic);
        app.delete('/usuaris/:id/bloquejat',usuaris.eliminarBloquejat);

        app.post('/partides', partides.create);
        app.get('/partides/:id', partides.getById);
        app.get('/partides', partides.getAll);
        app.put('/partides/:id/join', partides.join);
        app.put('/partides/:id/disjoin', partides.disjoin);
        app.get('/partides/:id/participants', partides.getAllParticipants);
        app.get('/partides/:id/ijoined', partides.iJoined);

        app.get("/", loggedIn);
        app.get("/index.html", loggedIn);

        var servidor = http.createServer(app).listen(app.get('port'), function () {
            console.log('Express server listening on port ' + app.get('port'))
            var sockets = new Sockets(servidor, sessionStore, cookieParser, db)
            partides.setSockets(sockets);
        })

    }, function (err) {
        console.log("Error initializing database: " + err);
    })
    .done();


