/**
 * Created by Javier on 02/04/2014.
 */

module.exports = function(db) {

    var uuid = require('node-uuid');
    var util = require('../util');
    var dao = require('../dao')(db);
    var jocController = require('../util/JocController');
    jocController = new jocController(db,null);
    var Sockets;

    return {
        setSockets : function(sockets) {
            Sockets = sockets;
        },

        create: function (req, res) {
            var nom = req.body.nom;
            var idUsuari = req.session.idusuari;
            var usuari = {id : idUsuari};

            if (!idUsuari) {
                util.stdSeqError(res, "No estas identificat");
                return;
            }

            if (!nom) {
                util.stdErr400(res, "Falta el nom");
                return;
            }

            var nMaxJugadors = req.body.max_jugadors;

            if (nMaxJugadors) {
                nMaxJugadors = parseInt(nMaxJugadors, 10);
                if (nMaxJugadors < 2 || nMaxJugadors > 6) {
                    util.stdErr400(res,"El numero de jugadors és incorrecte (entre 2 i 6)");
                    return;
                }
            }

            dao.Partida.getByNom(nom)
                .then(function (partida) {
                    if(partida) {
                        util.reject("Aquest nom de partida ja existeix");
                    } else {
                        dao.Partida.create(nom, nMaxJugadors)
                            .then(function(partida2) {
                                if (partida2) {
                                    return dao.JugadorPartida.create(usuari, partida2);
                                }
                            });
                    }
                })
                .then(util.stdSeqSuccess.genFuncLeft(res,{msg: "Partida creada"}), util.stdSeqError.genFuncLeft(res))
                .done();
        },

        getByNom : function(req, res) {
            var nom = req.params.nom;

            if (!nom) {
                util.stdErr400(res, "Missing 'nom' attribute");
                return;
            }

            dao.Partida.getByNom(nom)
                .then(function (partida) {
                    if(!partida) {
                        util.reject("No existeix la partida");
                    } else {
                        return partida;
                    }
                })
                .then(util.stdSeqSuccess.genFuncLeft(res), util.stdSeqError.genFuncLeft(res))
                .done();
        },

        getById: function (req, res) {
            if (!req.params.id) {
                util.stdErr500("Missing 'id' parameter");
                return;
            }

            dao.Partida.getById(req.params.id)
                .then(function (partida) {
                    if (!partida) util.reject("No Partida with 'id' = " + req.params.id);
                    else {
                        return partida;
                    }
                })
                .then(util.stdSeqSuccess.genFuncLeft(res), util.stdSeqError.genFuncLeft(res))
                .done();
        },

        getAll : function (req, res) {

            dao.Partida.getAll()
                .then(function(partides){
                    if (!partides) util.reject("No hi ha cap partida");
                    else return partides;
                })
                .then(util.stdSeqSuccess.genFuncLeft(res), util.stdSeqError.genFuncLeft(res))
                .done();

        },

        join : function(req, res) {
            var idPartida = req.params.id;
            var idUsuari = req.session.idusuari;

            if (!idUsuari) {
                util.stdErr400(res, "No estas identificat");
                return;
            }
            dao.Partida.getById(idPartida)
                .then(function (partida) {

                    if (partida) {

                        dao.JugadorPartida.numeroJugadors(partida)
                            .then(function (count) {

                                if (count < partida.nMaxJugadors) {
                                    dao.Usuari.getById(idUsuari)
                                        .then(function(usuari){
                                            dao.JugadorPartida.isJoinedUsuari(usuari, partida)
                                                .then(function(jp) {
                                                    if (jp)
                                                        util.stdErr500(res, "KO, ja estava a la partida");
                                                    else {
                                                        dao.JugadorPartida.create(usuari, partida)
                                                            .then(function(jpc){
                                                                if (jpc) {
                                                                    util.stdSeqSuccess(res, {msg: "OK", tots: (count+1==partida.nMaxJugadors)});
                                                                    if((count + 1) < partida.nMaxJugadors)
                                                                        Sockets.emitJoin(partida.id, usuari);
                                                                    else {
                                                                        Sockets.emitInici(partida.id);
                                                                        var sockets = Sockets.getAllSocketByPartida(partida.id);
                                                                        jocController.repartirCartes(partida.id,sockets);
                                                                    }

                                                                } else
                                                                    util.stdSeqError(res, "KO");
                                                            })
                                                            .done();
                                                    }
                                                })
                                                .done();
                                        })
                                        .done();
                                } else {
                                    util.stdErr500(res, "KO, massa gent");
                                }
                            })
                            .done();

                    } else {
                        util.stdErr400(res, "KO, No existeix la partida");
                    }
                })
                .done()

        },

        disjoin : function (req, res) {
            var idPartida = req.params.id;
            var idUsuari = req.session.idusuari;
            var usuari = {id : idUsuari};

            if (!idUsuari) {
                util.stdErr400(res, "No estas identificat");
                return;
            }

            dao.Partida.getById(idPartida)
                .then(function (partida) {

                    if (partida) {

                        dao.JugadorPartida.isJoinedUsuari(usuari, partida)
                            .then(function(jp) {
                                if (jp) {
                                    jp.destroy()
                                        .success(function() {
                                            dao.JugadorPartida.numeroJugadors(partida)
                                                .then(function(count){
                                                    if (count == 0) {
                                                        partida.destroy()
                                                            .success(function () {
                                                                util.stdSeqOk(res);
                                                            })
                                                            .error(function (){
                                                                util.stdSeqOk(res, "No s'ha pogut eliminar la partida amb 0 jugadors");
                                                            });
                                                    } else {
                                                        Sockets.emitDisjoin(partida.id, usuari);
                                                        util.stdSeqOk(res);
                                                    }
                                                });

                                        })
                                        .error(function(err) {
                                            util.stdErr400(res, "KO, no es pot eliminar " + err);
                                        });
                                }
                                else
                                   util.stdErr500(res, "KO, no hi era a la partida");
                            })
                            .done();

                    } else {
                        util.stdErr400(res, "KO, No existeix la partida");
                    }
                })
                .done()

        },

        getAllParticipants : function (req, res) {

            var idPartida = req.params.id;

            dao.Partida.getById(idPartida)
                .then(function (partida) {
                    if (!partida) util.reject("La partida no existeix");
                    else {
                        return dao.Partida.getAllParticipants(partida);
                    }
                })
                .then(util.stdSeqSuccess.genFuncLeft(res), util.stdSeqError.genFuncLeft(res))
                .done();
        },

        iJoined : function(req, res) {
            var idUsuari = req.session.idusuari;
            var idPartida = req.params.id;

            if (!idUsuari) {
                util.stdErr400(res, "No estas identificat");
                return;
            }

            var partida = {id: idPartida};
            var usuari = {id: idUsuari};
            dao.JugadorPartida.isJoinedUsuari(usuari, partida)
                .then(function(jp){
                    if (jp)
                        util.stdSeqSuccess(res, "true");
                    else
                        util.stdSeqSuccess(res, "false");
                })
                .done();
        }

    }

};