/**
 * Created by salvat on 01/04/14.
 */

module.exports = function(db) {

    var uuid = require('node-uuid');
    var util = require('../util');
    var dao = require('../dao')(db);

    return {
        create : function(req, res) {
            var nick=req.body.nick;
            var contrasenya=req.body.contrasenya;
            if (!nick && !contrasenya) {
                util.stdErr400(res, "Missing 'nick' or 'password' attribute");
                return;
            }
            var attribs={nick: nick, contrasenya:contrasenya}
            dao.Usuari.getByNick(nick)
                .then(function(usuari){
                    if(usuari) util.reject("Usuari ja existeix");
                    else return dao.Usuari.create(attribs);
                })
                .then(
                    function (u) {
                        req.session.idusuari = u.id;
                        req.session.nick = u.nick;
                        res.send("OK");
                    },
                    util.stdSeqError.genFuncLeft(res)
                )
                .done(); //util.stdSeqSuccess.genFuncLeft(res, {msg : "s'ha creat l'usuari"})
        },
        getById : function(req,res) {
            db.Usuari.find(req.params.id).success(function(usuari){
                res.setHeader('Content-Type','text-json');
                res.end(JSON.stringify(usuari));
            });
        },
        getUsuari : function(req,res){

            if (!req.params.id) {
                util.stdErr500("Missing 'id' parameter");
                return;
            }

            dao.Usuari.getUsuari(req.params.id)
                .then(function (usuari) {
                    if (!usuari) util.reject("No usuari with 'id' = " + req.params.id);
                    else {
                        return usuari;
                    }
                })
                .then(util.stdSeqSuccess.genFuncLeft(res), util.stdSeqError.genFuncLeft(res))
                .done();

        },
        modificarContrasenya : function (req, res){
            var contrasenyaVella=req.body.OldContrasenya;
            var contrasenyaNova=req.body.NewContrasenya;
            var id=req.params.id;
            //var attribs={contrasenya: contrasenyaNova}
            db.sequelize.transaction(function (t) {
                dao.Usuari.getById(id)
                    .then(function(usuari){
                        //si l'usuari existeix
                        if(usuari){
                            //comprovem la contrasenya vella sigui correcte
                            if(usuari.contrasenya==contrasenyaVella){
                                return dao.Usuari.update(usuari, contrasenyaNova, t);
                            }
                            else util.reject('Contrasenya Vella incorrecte');
                        }
                        else util.reject("Usuari no existeix");


                    })
                    .then(util.commit.genFuncLeft(t), util.rollback.genFuncLeft(t))
                    .then(util.stdSeqSuccess.genFuncLeft(res,'Contrasenya canviada!!'),
                    util.stdSeqError.genFuncLeft(res))
                    .done();
            });


        },

        login : function (req, res){
            var nick = req.body.nom;
            var pass = req.body.contrasenya;

            dao.Usuari.getByNick(nick)
                .then(function(usuari) {
                    if (!usuari) {
                        util.stdErr400(res, "KO");
                    } else {
                        if (usuari.contrasenya == pass) {
                            req.session.idusuari = usuari.id;
                            req.session.nick = usuari.nick;
                            res.send("OK");
                        } else {
                            util.stdErr400(res, "KO");
                        }
                    }
                })
                .done();
        },

        logout : function (req, res) {
            if (req.session.idusuari) {
                req.session.idusuari = null;
                req.session.nick = null;
                util.stdSeqOk(res);
            } else {
                res.stdSeqError(res, "No estas identificat");
            }
        },

        afegirAmic : function(req,res){
            var id = req.params.id;
            var idAmic = req.body.amic;
            dao.Usuari.getById(id)
                .then(function(usuari){
                    if(usuari){//usuari existeix
                        //comprovar que l'usuari amic també existeixi
                        dao.Usuari.getById(idAmic)
                            .then(function(usuariAmic){
                                if(usuariAmic){//usuari amic existeix

                                    return dao.Usuari.afegirAmic(usuari,usuariAmic);
                                }
                                else util.reject('Usuari Amic no existex');
                        })

                    }
                    else util.reject('Usuari no existeix');
                })
            .then(util.stdSeqSuccess.genFuncLeft(res,'Afegit amic correctament'),
            util.stdSeqError.genFuncLeft(res))
            .done();

        },
        getAll : function(req, res) {
                dao.Usuari.getAll()
                    .then(function(usuaris){
                        if (!usuaris) util.reject("No hi ha cap usuari ");
                        else return usuaris;
                    })
                    .then(util.stdSeqSuccess.genFuncLeft(res), util.stdSeqError.genFuncLeft(res))
                    .done();

        },
        cercaNick : function(req, res) {

            var c = req.query.cerca;

            dao.Usuari.getUsuaris(c)
                .then(function(usuaris){
                    if (!usuaris) util.reject("No hi ha cap usuari ");
                    else return usuaris;
                })
                .then(util.stdSeqSuccess.genFuncLeft(res), util.stdSeqError.genFuncLeft(res))
                .done();
        },
        afegirBloquejat : function(req,res){
            var id = req.params.id;
            var idBloq = req.body.bloquejat;
            dao.Usuari.getById(id)
                .then(function(usuari){
                    if(usuari){//usuari existeix
                        //comprovar que l'usuari amic també existeixi
                        dao.Usuari.getById(idBloq)
                            .then(function(usuariBloq){
                                if(usuariBloq){//usuari amic existeix

                                    return dao.Usuari.afegirBloquejat(usuari,usuariBloq);
                                }
                                else util.reject('Usuari Bloquejat no existex');
                            })

                    }
                    else util.reject('Usuari no existeix');
                })
                .then(util.stdSeqSuccess.genFuncLeft(res,'Afegit bloquejat correctament'),
                util.stdSeqError.genFuncLeft(res))
                .done();

        },
        eliminarAmic : function(req,res) {
            var id = req.params.id;
            var idAmic = req.body.amic;
            dao.Usuari.getById(id)
                .then(function (usuari) {
                    if (usuari) {//usuari existeix
                        //comprovar que l'usuari amic també existeixi
                        dao.Usuari.getById(idAmic)
                            .then(function (usuariAmic) {
                                if (usuariAmic) {//usuari amic existeix

                                    return dao.Usuari.eliminarAmic(usuari, usuariAmic);
                                }
                                else util.reject('Usuari Amic no existex');
                            })

                    }
                    else util.reject('Usuari no existeix');
                })
                .then(util.stdSeqSuccess.genFuncLeft(res, 'Eliminat amic correctament'),
                util.stdSeqError.genFuncLeft(res))
                .done();
        },
        eliminarBloquejat : function(req,res) {
            var id = req.params.id;
            var idBloq = req.body.bloquejat;
            dao.Usuari.getById(id)
                .then(function (usuari) {
                    if (usuari) {//usuari existeix
                        //comprovar que l'usuari amic també existeixi
                        dao.Usuari.getById(idBloq)
                            .then(function (usuariBloq) {
                                if (usuariBloq) {//usuari amic existeix

                                    return dao.Usuari.eliminarBloquejat(usuari, usuariBloq);
                                }
                                else util.reject('Usuari Bloquejat no existex');
                            })

                    }
                    else util.reject('Usuari no existeix');
                })
                .then(util.stdSeqSuccess.genFuncLeft(res, 'Eliminat Bloquejat correctament'),
                util.stdSeqError.genFuncLeft(res))
                .done();
        }

    }
}
