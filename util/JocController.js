/**
 * Created by Javier on 19/05/2014.
 */

var Carta = require("./Carta");
var W = require('when');

function JocController (db, socket, sio) {
    this.dao = require('../dao')(db);
    this.socket = socket;
    this.sio = sio;
}

JocController.prototype.repartirCartes = function (idPartida,sockets) {
    var cartes = (new Carta()).generarCartes();

    var barrejades = this.shuffle(cartes);
    console.log(JSON.stringify(barrejades));

    var that = this;
    this.dao.Partida.getById(idPartida)
        .then(function (partida){
            if(partida){
                that.dao.Partida.getAllParticipants(partida)
                    .then(function (jugadors) {
                        var promises = [];

                        var contrincants = [];
                        for (var i = 0; i < partida.nMaxJugadors; i++) {
                            var jugador = jugadors[i].usuari;
                            contrincants.push({idUsuari : jugador.id, nick : jugador.nick, nCartes : 7});
                        }

                        var r = Math.floor(Math.random() * jugadors.length);
                        var primer = jugadors[r].usuari.id;

                        for(var i = 0; i < partida.nMaxJugadors; i++){
                            var jugador = jugadors[i];
                            if (!jugador) break; //per fer debug
                            jugador = jugador.usuari;
                            var h = barrejades.splice(0, 7);

                            for(var s = 0; s<sockets.length;s++){
                                var idSocketUsuari=sockets[s];
                                if(idSocketUsuari.idUsuari ==jugador.id){
                                    idSocketUsuari.emit("cartesRepartides",{cartesRepartides : h, idUsuari : idSocketUsuari.idUsuari, nick: idSocketUsuari.nick, usuaris: contrincants, jugadorTirar: primer});
                                    break;
                                }
                            }


                            //console.log(JSON.stringify(h));
                            promises.push(that.dao.JugadorPartida.inici(partida, jugador, i, that.stringfyCartes(h)));

                        }
                        //promises.push(that.dao.Partida.updateNoRepartides(partida, that.stringfyCartes(barrejades)));
                        promises.push(that.dao.Partida.setInici(partida, primer));

                        W.all(promises)
                            .then(function(res) {
                                console.log("OK");
                            })
                            .done();

                    })
                    .done();
            }
        })
        .done();

};

JocController.prototype.stringfyCartes = function (a) {
    var str = "";
    for(var i = 0; i < a.length; i++) {
        str += a[i];
        if (i < (a.length - 1))
            str += ":";
    }
    return str;
};

JocController.prototype.string2cartes = function (str) {
    if (str.charAt(0) == ":")
        str = str.substring(1, str.length);

    if (str == "") return [];

    var p = str.split(":");
    var x = [];
    for (var i = 0; i < p.length; i++) {
        if (p[i] != "") {
            x.push(p[i]);
        }
    }

    return x;
};



JocController.prototype.shuffle = function (o){
    for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
    return o;
};

JocController.prototype.tirarCarta = function (idPartida, idUsuari, c, color) {
    var p = {id : idPartida};
    var u = {id : idUsuari};
    var promises = [
        this.dao.Partida.getById(idPartida),
        this.dao.JugadorPartida.getByPartidaAndUsuari(p, u),
        this.dao.JugadorPartida.getByPartida(p)
        //this.dao.Usuari.getById(idUsuari)
    ];
    var that = this;
    var carta = new Carta(c);
    W.all(promises)
        .then(function (resultats){

            var partida = resultats[0];
            var jugador = resultats[1];
            var jugadors = resultats[2];
            //var usuari = resultats[3];


            if (partida && jugador) {

                var us = partida.getUsuari();
                if (partida.UsuariId == idUsuari) {

                    if (that.teLaCarta(jugador, carta)) {

                        var ultima = that.ultimaCarta(partida);
                        if(partida.cartesEmpassades!=0){
                            if(carta.isMes2()){
                                var jugadorMatat=that.getSeguentJugador(partida, jugadors);
                                if(that.seguimSuman2Cartes(jugadorMatat)){
                                    partida.setDataValue("cartesEmpassades", partida.cartesEmpassades+2);
                                    partida.setDataValue("colorActual",carta.color);
                                    that.continuar(partida, jugador,jugadors,carta);
                                }
                                else{

                                    that.afegirCartesRandom(jugadorMatat,partida.cartesEmpassades+2);
                                    partida.setDataValue("cartesEmpassades",0);
                                    that.passarJugador(partida,jugador,jugadors);
                                    partida.setDataValue("colorActual",carta.color);
                                    that.continuar(partida, jugador,jugadors,carta);
                                }
                            }
                            else{
                                console.log("Clicka el +2");
                                that.socket.emit('error',{error: "Error, has de tirar el +2"});
                                }
                        }
                        else if (carta.isCanviColor()) {
                            if (partida.colorActual) {
                                if (color) {
                                    partida.setDataValue("colorActual", color); //canvi color

                                    that.continuar(partida, jugador,jugadors,carta, color);
                                } else {

                                    that.socket.emit('error',{error: "Error: no has dit el color"});
                                }
                            } else {

                                that.socket.emit('error',{error: "Error no pots començar amb un comodi canvi color"});
                            }

                        } else if (carta.isMes4()) {
                            //afegir al seguent +4
                            if (color) {
                                if (partida.colorActual) {
                                    partida.setDataValue("colorActual", color); //canvi color
                                    var jugadorMatat=that.getSeguentJugador(partida, jugadors);
                                    that.afegirCartesRandom(jugadorMatat,4);
                                    that.passarJugador(partida,jugador,jugadors);
                                    that.continuar(partida, jugador,jugadors,carta, color);
                                } else {

                                    that.socket.emit('error',{error: "Error no pots començar amb un comodi +4"});
                                }
                            } else {

                                that.socket.emit('error',{error: "Error: no has dit el color"});
                            }



                        } else if (!partida.colorActual || partida.colorActual == carta.getColor()) {
                            if (carta.isCanviSentit()) {
                                that.CanviSentit(partida);
                                that.continuar(partida, jugador,jugadors,carta);

                            } else if(carta.isPassar()) {
                                that.passarJugador(partida,jugador,jugadors);
                                that.continuar(partida, jugador,jugadors,carta);
                            } else if(carta.isMes2()) {
                                var jugadorMatat=that.getSeguentJugador(partida,jugadors);
                                if(that.seguimSuman2Cartes(jugadorMatat)){
                                    partida.setDataValue("cartesEmpassades", partida.cartesEmpassades+2);
                                    partida.setDataValue("colorActual",carta.color);
                                    that.continuar(partida, jugador,jugadors,carta);
                                }
                                else{
                                    that.afegirCartesRandom(jugadorMatat,partida.cartesEmpassades+2);
                                    partida.setDataValue("cartesEmpassades",0);
                                    partida.setDataValue("colorActual",carta.color);
                                    that.passarJugador(partida,jugador,jugadors);
                                    that.continuar(partida, jugador,jugadors,carta);
                                }

                            } else {//es un numero
                                that.continuar(partida, jugador, jugadors,carta);

                            }
                        } else if (ultima && carta.tipus == ultima.tipus ) {
                            partida.setDataValue("colorActual", carta.getColor());
                            that.continuar(partida, jugador,jugadors,carta);
                        } else {

                            that.socket.emit('error',{error: "Error carta no correcte"});
                        }
                    } else {
                        that.socket.emit('error',{error: "Error no tens la carta"});
                    }


                } else {
                    that.socket.emit('error',{error: "Error no tens el torn"});
                }
            } else {
                that.socket.emit('error',{error: "Error en les consultes SQL"});
            }

        })
        .done();

};
JocController.prototype.seguimSuman2Cartes = function(jugadorMatat){
    var cartes = this.string2cartes(jugadorMatat.cartes);
    var trobat = false;
    var i=0;
    while (!trobat && i<cartes.length){
        if(cartes[i]=="RD"||cartes[i]=="GD"||cartes[i]=="BD"||cartes[i]=="YD"){
            trobat=true;
        }
        i++;
    }
    return trobat;
}
JocController.prototype.CanviSentit = function(partida){
    partida.setDataValue('sentitHorari',(!partida.sentitHorari));
};
JocController.prototype.teLaCarta = function (jugador, carta) {
    var cartes = this.string2cartes(jugador.cartes);

    return cartes.indexOf(carta.valor) >= 0;
};


JocController.prototype.ultimaCarta = function (partida) {

    var tirades = partida.cartesTirades;
    if (tirades) {
        var a = this.string2cartes(tirades);
        if (a.length > 0) {
            return new Carta(a[a.length - 1]);
        }
    }

    return false;
};
JocController.prototype.passarJugador = function (partida, jugadorAct, jugadors){
    var sentit = partida.sentitHorari;//TRUE horari False antihorari
    var nouJugAct;
    if (sentit) nouJugAct = (jugadorAct.ordre+1)%(partida.nMaxJugadors);
    else {
        if(jugadorAct.ordre==0)
        nouJugAct=partida.nMaxJugadors-1;
        else
        nouJugAct = (jugadorAct.ordre-1)%(partida.nMaxJugadors);

    }
    partida.setDataValue("UsuariId", jugadors[nouJugAct].UsuariId);
};
JocController.prototype.afegirCartesRandom = function(jugador,quantes){
    var cartes= (new Carta()).generarCartes();
    cartes=this.shuffle(cartes);
    var cartesJug=jugador.cartes;
    cartesJug = this.string2cartes(cartesJug);
    for(var i=0;i<quantes;i++){
       cartesJug.push(cartes[i]);
    }
    var cartesJugA=this.stringfyCartes(cartesJug);
    jugador.setDataValue("cartes", cartesJugA);

    var sockets = this.sio.getAllSocketByPartida(jugador.PartidaId);
    for(var s = 0; s<sockets.length;s++){
        var idSocketUsuari=sockets[s];
        if(idSocketUsuari.idUsuari ==jugador.UsuariId){
            idSocketUsuari.emit("afegirCartes",{cartes : cartesJug});
            break;
        }
    }

}

JocController.prototype.getSeguentJugador = function (partida, jugadors) {
    var sentit = partida.sentitHorari;
    var nouJugAct;

    var idUsuari = partida.getDataValue("UsuariId");

    var jugadorAct;

    for (var i = 0; i < jugadors.length; i++) {
        var c3po = jugadors[i];
        if (c3po.UsuariId == idUsuari) {
            jugadorAct = c3po;
            break;
        }
    }

    var ordre = jugadorAct.getDataValue("ordre");

    if (sentit) nouJugAct = (ordre + 1) % (partida.nMaxJugadors);
    else nouJugAct = Math.abs((ordre - 1) % (partida.nMaxJugadors));

    return jugadors[nouJugAct];
};

JocController.prototype.continuar = function (partida, jugadorActual, jugadors, carta, color) {

    this.treureCarta(jugadorActual, carta);
    var seguent = this.getSeguentJugador(partida, jugadors);
    partida.setDataValue("UsuariId", seguent.UsuariId);

    var tirades = this.string2cartes(partida.cartesTirades);
    tirades.push(carta.toString());
    partida.setDataValue("cartesTirades", this.stringfyCartes(tirades));

    if (!partida.colorActual) {
        partida.setDataValue("colorActual", carta.getColor());
    }



    var contrincants = [];

    for (var i = 0; i < jugadors.length; i++) {
        var j = jugadors[i];
        var w = 0;
        if (j.UsuariId == jugadorActual.UsuariId) {}
            w = -1;

        contrincants.push({idUsuari: j.UsuariId, nCartes: this.comptarCartes(j.getDataValue("cartes")) + w})
    }

    partida.save();


    if (this.comptarCartes(jugadorActual.cartes) == 0) {
        if (jugadorActual.uno) {
            this.socket.emitFi(partida.id, jugadorActual.UsuariId, carta.toString());

            this.dao.Partida.getAllParticipants(partida)
                .then(function (usuaris) {

                    usuaris.forEach(function(j) {
                        var jj = j.usuari;
                        if (jj.id == jugadorActual.UsuariId) {
                            jj.setDataValue("guanyades", jj.guanyades + 1);
                        } else {
                            jj.setDataValue("perdudes", jj.perdudes + 1);
                        }
                        jj.save();
                    })

                    jugadors.forEach(function (act){
                        act.destroy();
                    });

                    partida.setDataValue("comensada", false);
                    partida.save();
                })
                .done();


            return;
        }
    }

    if (this.comptarCartes(seguent.cartes) == 1 && !seguent.uno) {
        this.afegirCartesRandom(seguent, 2);
    }

    jugadorActual.save();
    for (var i = 0; i < jugadors.length; i++) {
        var j = jugadors[i];

        if (j.UsuariId != jugadorActual.UsuariId) {
            j.save();
        }
    }

    this.socket.emitContinua(partida.id,carta.toString(),partida.colorAct,seguent.UsuariId, contrincants, color);


};

JocController.prototype.treureCarta = function (jugador, carta) {
    var luke = jugador.cartes.replace(carta.toString(), "");
    luke = luke.replace("::", ":");
    jugador.setDataValue('cartes', luke);
    //jugador.save();
};

JocController.prototype.agafarCartes = function(idPartida, idUsuari) {
    var p = {id : idPartida};
    var u = {id : idUsuari};
    var promises = [
        this.dao.Partida.getById(idPartida),
        this.dao.JugadorPartida.getByPartidaAndUsuari(p, u)
    ];
    var that = this;

    W.all(promises)
        .then(function (resultats){
            var partida = resultats[0];
            var usuari = resultats[1];

            if (partida && usuari) {
                that.afegirCartesRandom(usuari, 1);
                usuari.save();
            } else {
                that.socket.emit('error',{error: "Error en les consultes SQL"});
            }

        })
        .done();
};

JocController.prototype.accioUno = function (idPartida, idUsuari) {
    var p = {id : idPartida};
    var u = {id : idUsuari};
    var promises = [
        this.dao.Partida.getById(idPartida),
        this.dao.JugadorPartida.getByPartidaAndUsuari(p, u),
        this.dao.JugadorPartida.getByPartida(p)
    ];
    var that = this;

    W.all(promises)
        .then(function (resultats) {
            var partida = resultats[0];
            var usuari = resultats[1];

            if (partida && usuari) {
                console.log("uno!")
                if (that.comptarCartes(usuari.cartes) == 1) {
                    usuari.setDataValue("uno", true);
                    usuari.save();
                    that.socket.emitUno(partida.id, that.socket.nick, that.socket.idUsuari);
                } else {
                    that.socket.emit('error',{error: "No pots cridar UNO! borinot"});
                }

            } else {
                that.socket.emit('error',{error: "Error en les consultes SQL"});
            }
        })
        .done();
};


JocController.prototype.comptarCartes = function (strCartes) {
    if (!strCartes) return 0;

    var c = this.string2cartes(strCartes);
    return c.length;
};
module.exports = JocController;