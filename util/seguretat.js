/**
 * Created by Javier on 09/04/2014.
 */



module.exports = function (db) {

    var S = {};
    var W = require('when');
    var dao = require('../dao')(db);

    S.checkUser = function (id, contrasenya) {
        var df = W.defer();
        dao.Usuari.getByNick(id) //TODO getById
            .then(function(usuari){

                if (usuari) {
                    if (usuari.contrasenya == contrasenya) {
                        df.resolve(usuari);
                    } else {
                        df.reject(false);
                    }
                } else {
                    df.reject(false);
                }

            }, function(e) {
                df.reject(false);
            })
            .done();

        return df.promise;
    };

    return S;

}

