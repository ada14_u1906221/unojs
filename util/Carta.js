/**
 * Created by Javier on 25/05/2014.
 */

function Carta(valor) {
    if (valor) {
        this.valor = valor;
        this.color = valor.charAt(0);
        this.tipus = valor.charAt(1);
    }
}

Carta.prototype.RED = "R";
Carta.prototype.GREEN = "G";
Carta.prototype.BLUE = "B";
Carta.prototype.YELLOW = "Y";
Carta.prototype.BLACK = "N";
Carta.prototype.COLORS = [Carta.prototype.RED, Carta.prototype.GREEN, Carta.prototype.BLUE, Carta.prototype.YELLOW];

Carta.prototype.V0 = "0";
Carta.prototype.V1 = "1";
Carta.prototype.V2 = "2";
Carta.prototype.V3 = "3";
Carta.prototype.V4 = "4";
Carta.prototype.V5 = "5";
Carta.prototype.V6 = "6";
Carta.prototype.V7 = "7";
Carta.prototype.V8 = "8";
Carta.prototype.V9 = "9";
Carta.prototype.INVERTIR = "I";
Carta.prototype.MES_2 = "D";
Carta.prototype.SALTAR = "S";
Carta.prototype.CANVI_COLOR = "C";
Carta.prototype.MES_4 = "Q";


Carta.prototype.VALORS = [
    Carta.prototype.V0,
    Carta.prototype.V1,
    Carta.prototype.V2,
    Carta.prototype.V3,
    Carta.prototype.V4,
    Carta.prototype.V5,
    Carta.prototype.V6,
    Carta.prototype.V7,
    Carta.prototype.V8,
    Carta.prototype.V9,
    Carta.prototype.INVERTIR,
    Carta.prototype.MES_2,
    Carta.prototype.SALTAR
];

Carta.prototype.generarCartes = function () {

    var comodins = [Carta.prototype.CANVI_COLOR, Carta.prototype.MES_4];
    var cartes = [];
    for (var r = 0; r < 2; r++) {

        for (var i = 0; i < Carta.prototype.COLORS.length; i++) {

            for (var j = 0; j < Carta.prototype.VALORS.length; j++) {
                cartes.push(Carta.prototype.COLORS[i] + Carta.prototype.VALORS[j]);
            }
        }

        for (var e = 0; e < comodins.length; e++) {
            cartes.push(Carta.prototype.BLACK + comodins[e]);
        }
    }

    return cartes;
};

Carta.prototype.isCanviColor = function() {
    return this.color == Carta.prototype.BLACK && this.tipus == Carta.prototype.CANVI_COLOR;
};

Carta.prototype.isMes4 = function() {
    return this.color == Carta.prototype.BLACK && this.tipus == Carta.prototype.MES_4;
};

Carta.prototype.getColor = function () {
    return this.color;
};

Carta.prototype.isCanviSentit = function () {
    return this.color != Carta.prototype.BLACK && this.tipus == Carta.prototype.INVERTIR;
};

Carta.prototype.isPassar = function () {
    return this.color != Carta.prototype.BLACK && this.tipus == Carta.prototype.SALTAR;
};

Carta.prototype.isMes2 = function () {
    return this.color != Carta.prototype.BLACK && this.tipus == Carta.prototype.MES_2;
};

Carta.prototype.isNumber = function () {
    return !isNaN(this.tipus);
};

Carta.prototype.toString = function () {
    return this.color + this.tipus;
};

module.exports = Carta;