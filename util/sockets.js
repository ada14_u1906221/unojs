/**
 * Created by Javier on 14/05/2014.
 */

function Sockets(servidor, sessionStore, cookieParser, db) {
    var dao = require('../dao')(db);
    this.sio = require('socket.io').listen(servidor);
    var JocController = require('./JocController');
    var SessionSockets = require('session.socket.io')
        , sessionSockets = new SessionSockets(this.sio, sessionStore, cookieParser);
    this.sio.set('log level', 1);
    var that  = this;

    sessionSockets.on('connection', function (err, socket, session) {

        if (!session || !session.idusuari) {
            console.log("Inici de socket sense autoritzacio!");
            socket.disconnect('unauthorized');
            return;
        }

        var idUsuari = session.idusuari;
        var nick = session.nick;
        var usuari = {id: idUsuari, nick : nick};
        var joc = new JocController (db,socket, that);
        socket.idUsuari = idUsuari;

        socket.on('join', function(data){
            var idPartida = data.idPartida;
            that.checkIsJoined(usuari, data,
                function(jp){
                    if(jp) {
                        console.log("join: " + idPartida);
                        socket.join(that.getNameRoomPartida(idPartida));
                    } else {
                        console.log("join error: " + idPartida);
                    }
                });

        });

        socket.on('disjoin', function(data){
            var idPartida = data.idPartida;
            console.log("disjoin: " + JSON.stringify(data));
            socket.leave(that.getNameRoomPartida(idPartida));
        });

        socket.on('agafarCartes', function(data){

            var idPartida = data.idPartida;
            joc.agafarCartes(idPartida, idUsuari);

        });

        socket.on('xat', function(data){
            //data -> idUsuari i msg
            var idPartida = data.idPartida;
            data.nick=nick;
            data.idUsuari=idUsuari;
            that.checkIsJoined(usuari, data,
                function(jp){
                    if(jp) {
                        console.log("xat: " + JSON.stringify(data));
                        that.getRoomPartida(idPartida).emit('xat', data);
                    } else {
                        console.log("Error xat: " + JSON.stringify(data));
                    }
                }
            );

        });

        socket.on("tirarCarta", function(data){
            //comprovar si es el seu torn
            //comprovar si te la carta
            //comprovar si es carta valida
            //tirarla
            joc.tirarCarta(data.idPartida, idUsuari, data.carta, data.color);
        });

        socket.on("uno", function(data) {
            //comprovar si el torn es socket.jugador + 1
            //comprovar si te 1 carta
            //desar uno!
            //emit
            joc.accioUno(data.idPartida, idUsuari);
        });

        socket.on("disconnect", function() {
            //avisar als de la partida??
            console.log("Usuari desconectat: " + nick);
        });

        socket.on('news', function (data) {
            console.log(data);
            socket.emit('my other event', { my: 'data' });
        });

        socket.emitContinua = function(idPartida,carta,colorAct,jugadorTirar, contrincants,color) {
            if(!color)
                getRoomPartida(idPartida).emit('continua', {carta: carta, colorAct: colorAct, jugadorTirar: jugadorTirar, usuaris: contrincants});

            else
                getRoomPartida(idPartida).emit('continua', {carta: carta, colorAct: colorAct, jugadorTirar: jugadorTirar, usuaris: contrincants, color : color});

        };

        socket.emitUno = function(idPartida,nick, idUsuari) {
            getRoomPartida(idPartida).emit('uno', {nick: nick, idUsuari: idUsuari})
        };

        socket.emitFi = function(idPartida, guanyador, carta) {
            var room = getRoomPartida(idPartida);
            room.emit('fi', {idUsuari: guanyador, carta : carta});
            var clients = room.clients();
            for (var i = 0; i < clients.length; i++) {
                var c = clients[i];
                c.leave("" + idPartida);
            }
        };
        function getRoomPartida(idPartida) {
            return that.sio.sockets.in("" + idPartida);
        };

    });

    this.getNameRoomPartida = function(idPartida) {
        return "" + idPartida;
    };

    this.getRoomPartida = function(idPartida) {
        return this.sio.sockets.in(this.getNameRoomPartida(idPartida));
    };

    this.getPartida = function(data) {
        return {id: data.idPartida};
    };

    this.checkIsJoined = function(usuari, data, next) {
        dao.JugadorPartida.isJoinedUsuari(usuari, that.getPartida(data))
            .then(next)
            .done()
    };
}


Sockets.prototype.emitInici = function(idPartida) {

    this.getRoomPartida(idPartida).emit('inici', []);
};

Sockets.prototype.emitFi = function(idPartida, guanyador) {
    this.getRoomPartida(idPartida).emit('fi', {idUsuari: guanyador});
};

Sockets.prototype.emitJoin = function(idPartida, usuari) {
    this.getRoomPartida(idPartida).emit("nouJoin", {idUsuari : usuari.id, nick : usuari.nick});
};

Sockets.prototype.emitDisjoin = function(idPartida, usuari) {
    this.getRoomPartida(idPartida).emit("nouDisjoin", {idUsuari : usuari.id});
};

Sockets.prototype.emitContinua = function(idPartida,carta,colorAct,jugadorTirar, contrincants, color) {
    if (color)
        this.getRoomPartida(idPartida).emit('continua', {carta: carta, colorAct: colorAct, jugadorTirar: jugadorTirar, usuaris: contrincants});
    else
        this.getRoomPartida(idPartida).emit('continua', {carta: carta, colorAct: colorAct, jugadorTirar: jugadorTirar, usuaris: contrincants, color: color});
};

Sockets.prototype.emitUno = function(idPartida,nick, idUsuari) {
    this.getRoomPartida(idPartida).emit('uno', {nick: nick, idUsuari: idUsuari});
};

Sockets.prototype.getAllSocketByPartida = function(idPartida) {
    return this.getRoomPartida(idPartida).clients();
};

module.exports = Sockets;