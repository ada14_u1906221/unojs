/**
 * Created by salvat on 31/03/14.
 */

module.exports = function(sequelize, DataTypes) {
    var Bloquejat = sequelize.define('Bloquejat', {

    }, {
        classMethods : {
            associate : function(models) {
                //Client.hasMany(models.Shop)
               // Bloquejat.belongsTo(models.Usuari);
                //Bloquejat.belongsTo(models.Usuari,{as: 'UsuariId2'});
                Bloquejat.belongsTo(models.Usuari,{as: 'UsuariId1',foreignKey: 'UsuariId1'});
                Bloquejat.belongsTo(models.Usuari,{as: 'UsuariId2',foreignKey: 'UsuariId2'});
            }
        }
    });

    return Bloquejat;
};
