/**
 * Created by salvat on 01/04/14.
 */

module.exports = function(sequelize, DataTypes) {
    var JugadorPartida = sequelize.define('JugadorPartida', {
        cartes : DataTypes.TEXT,
        punts : { type : DataTypes.INTEGER, defaultValue : 0},
        ordre : DataTypes.INTEGER,
        uno : {type: DataTypes.BOOLEAN, defaultValue: false}
    }, {
        classMethods : {
            associate : function(models) {

                JugadorPartida.belongsTo(models.Partida);
                JugadorPartida.belongsTo(models.Usuari);
            }
        }
    });

    return JugadorPartida;
};
