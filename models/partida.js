/**
 * Created by salvat on 01/04/14.
 */

module.exports = function(sequelize, DataTypes) {
    var Partida = sequelize.define('Partida', {
        nom : {type : DataTypes.STRING(255), unique : true},
        //public : { type : DataTypes.BOOLEAN, defaultValue : false },
        nMaxJugadors : { type : DataTypes.INTEGER, defaultValue : 2 },
        sentitHorari : { type : DataTypes.BOOLEAN, defaultValue : true },
        acabada : { type : DataTypes.BOOLEAN, defaultValue: false},
        comensada : { type : DataTypes.BOOLEAN, defaultValue: false},
        //cartesNoRepartides : DataTypes.TEXT,
        cartesTirades : DataTypes.TEXT,
        colorActual : DataTypes.STRING(1),
        cartesEmpassades : DataTypes.INTEGER
    }, {
        classMethods : {
            associate : function(models) {

                Partida.belongsTo(models.Usuari);

                Partida.hasMany(models.Usuari, { through: models.JugadorPartida });
            }
        }
    });

    return Partida;
};
