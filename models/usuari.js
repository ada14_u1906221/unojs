/**
 * Created by salvat on 31/03/14.
 */

module.exports = function(sequelize, DataTypes) {
    var Usuari = sequelize.define('Usuari', {
        nick : DataTypes.STRING(255),
        contrasenya : DataTypes.STRING(255),
        ultimaConexio : DataTypes.STRING(512),
        guanyades : {type: DataTypes.INTEGER, defaultValue: 0},
        perdudes : {type: DataTypes.INTEGER, defaultValue: 0},
        nivell : DataTypes.STRING(255)
    }, {
        classMethods : {
            associate : function(models) {
                //Usuari.hasMany(models.bloquejat);
                Usuari.hasMany(models.Partida, { through: models.JugadorPartida });
            }
        }
    });

    return Usuari;
};
