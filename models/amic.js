/**
 * Created by salvat on 31/03/14.
 */

module.exports = function(sequelize, DataTypes) {
    var Amic = sequelize.define('Amic', {

    }, {
        classMethods : {
            associate : function(models) {
                //Client.hasMany(models.Shop)
                Amic.belongsTo(models.Usuari,{as: 'UsuariId1',foreignKey: 'UsuariId1'});
                Amic.belongsTo(models.Usuari,{as: 'UsuariId2',foreignKey: 'UsuariId2'});
            }
        }
    });

    return Amic;
};
